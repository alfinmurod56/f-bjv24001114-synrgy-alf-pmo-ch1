package Chapter1;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

@Data
@Setter @Getter

public class Challange1 {

    private static final Map<String, Integer> menu = new LinkedHashMap<>();
    static {
        menu.put("Nasi Goreng", 15000);
        menu.put("Mie Goreng", 13000);
        menu.put("Nasi + Ayam", 18000);
        menu.put("Es Teh Manis", 3000);
        menu.put("Es Jeruk", 5000);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int pilihan;
        Map<String, Integer> order = new HashMap<>();
        int totalQty = 0; // Variabel untuk menyimpan total qty pesanan

        System.out.println("==========================");
        System.out.println("Selamat datang di BinarFud");
        System.out.println("==========================");
        do {
            pilihan = -1;
            System.out.println("=> ");
            int count = 1;
            for (Map.Entry<String, Integer> entry : menu.entrySet()){
                System.out.println(count + ". " + entry.getKey() + "\t| " +entry.getValue());
                count++;
            }
            System.out.println("99. Pesan dan Bayar");
            System.out.println("0. Keluar aplikasi");
            System.out.print("\n=> ");

            if(scanner.hasNextInt()){
                pilihan = scanner.nextInt();
                scanner.nextLine();
            }
            else {
                System.out.println("Input tidak valid");
                if (scanner.hasNext()){
                    String notIntValue = scanner.next();
                    System.out.println("Input is not an Integer: " +notIntValue);

                }else {
                    System.out.println("No More Input");
                }
                scanner.nextLine();
            }

            if (pilihan >= 1 && pilihan <= menu.size()){
                String pilihItem = (String) menu.keySet().toArray()[pilihan - 1];
                int harga = menu.get(pilihItem);
                order.put(pilihItem, harga);
                System.out.println("\n=================");
                System.out.println("Berapa pesanan anda");
                System.out.println("=================\n");
                System.out.println(pilihItem + "\t| " + harga);
                System.out.println("(input 0 untuk kembali)");
                System.out.print("\nQty=> ");

                try {
                    int qty = Integer.parseInt(scanner.next());
                    if(qty == 0){
                        order.remove(pilihItem);
                    }else{
                        totalQty += qty; // Menambahkan qty ke totalQty
                        order.put(pilihItem, harga * qty);
                    }
                }catch (NumberFormatException e){
                    System.out.println("Input tidak valid");
                }
            }else if (pilihan == 99){
                confirmOrder(order, totalQty); // Mengirim totalQty ke metode confirmOrder
                order.clear();
                totalQty = 0; // Mereset totalQty setelah pesanan dikonfirmasi
            }
        }while (pilihan!=0);
        System.exit(0);
    }

    public static void confirmOrder(Map<String, Integer> order, int totalQty) {
        int total = 0;

        System.out.println("========================");
        System.out.println("Konfirmasi & Pembayaran");
        System.out.println("========================\n");
        for (Map.Entry<String, Integer>entry : order.entrySet()){
            System.out.println(entry.getKey() + "\t\t" + (entry.getValue() / menu.get(entry.getKey())) + "\t" + entry.getValue());
            total += entry.getValue();
        }

        System.out.println("---------------------------+");
        System.out.println("Total\t\t\t"+ totalQty + "\t"+ total);
//        System.out.println("Total Qty\t\t" + totalQty); // Menampilkan totalQty
        System.out.println("1. Konfirmasi dan Bayar");
        System.out.println("2. Kembali ke menu utama");
        System.out.println("0. Keluar Aplikasi");
        System.out.print("\n=> ");

        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();

        switch (choice){
            case 1:
                try {
                    FileWriter writer = new FileWriter("struk.txt");
                    writer.write("==================\n");
                    writer.write("BinarFud\n");
                    writer.write("==================\n");
                    writer.write("Terimkasih sudah memesan di BinarFud\n\n");
                    writer.write("Dibawah ini adalah pesanan anda\n\n");
                    for (Map.Entry<String, Integer> entry : order.entrySet()){
                        writer.write(entry.getKey() + "\t" + (entry.getValue() / menu.get(entry.getKey())) + "\t" + entry.getValue() + "\n");
                    }
                    writer.write("-----------------------+\n");
                    writer.write("Total\t\t"+ totalQty +"\t"+ total + "\n");
//                    writer.write("Total Qty\t\t\t" + totalQty + "\n"); // Menampilkan totalQty
                    writer.write("\nPembayaran : BinarCash\n\n");
                    writer.write("==========================\n");
                    writer.write("Simpan struk ini sebagai\nbukti pembayaran\n");
                    writer.write("==========================\n");
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.exit(0);
                break;
            case 2:
                break;
            case 0:
                System.exit(0);
                break;
            default:
                System.out.println("input tidak valid");
        }
    }
}

